package com.battery.dnd;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.battery.ubertestapp.R;

/**
 * Created by namrata on 3/2/15.
 */
public class SelectDndPlanActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_dnd_plan);
        ListView mylist = (ListView) findViewById(R.id.plan_list);
        String[] list={"one","two","three"};
        ArrayAdapter adapter = new ArrayAdapter<String>(SelectDndPlanActivity.this,android.R.layout.simple_list_item_multiple_choice,list);
        mylist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        mylist.setAdapter(adapter);
        mylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // do something here
            }
        });
    }
}
