package com.battery.dnd;

import android.net.Uri;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 3/2/15.
 */
public class DndStatusApi {

    public static String number = DndActivity.number;

    public String GET(String url) throws Exception{
        //create HTTPClient
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        request.setHeader("X-Mashape-Key" ,"TkXq4BmmSjmshFqXdjGOVS3GIa5Cp18Wz7Ljsnn3MQsDhZWCEy");
        request.setHeader("Accept" , "application/json");

        HttpResponse httpResponse = httpclient.execute(request);

        System.out.println("Response Code : "
                + httpResponse.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(httpResponse.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine())!=null){
            result.append(line);
        }
        return result.toString();
    }

    public List<DndData> dndDataList() throws Exception{
        List <DndData> dndDatas = new ArrayList<DndData>();
        Uri.Builder builder = Uri.parse("https://dndcheck.p.mashape.com/index.php").buildUpon();
        builder.appendQueryParameter("mobilenos" , number);

        String url = builder.build().toString();
        String outputData = GET(url);
        System.out.println(url);
        System.out.println(outputData);
        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(outputData);
            JSONObject info = jsonArray.getJSONObject(0);
            DndData dndData = new DndData();
            dndData.setMobileNumber(info.getString("mobilenumber"));
            dndData.setDndStatus(info.getString("DND_status"));
            dndDatas.add(dndData);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dndDatas;
    }
}
