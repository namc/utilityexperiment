package com.battery.dnd;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 3/2/15.
 */

public class DndActivity extends Activity {

    public static String number;
    TextView dndNumber;
    TextView status;
    Button activate;
    DndStatusApi dndStatusApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dnd);
        Intent intent = getIntent();
        number = intent.getStringExtra("number");
        dndNumber = (TextView) findViewById(R.id.dnd_number);
        status = (TextView) findViewById(R.id.dnd_status);
        activate = (Button) findViewById(R.id.activate);
        activate.setVisibility(View.INVISIBLE);
        new DndTask().execute();
    }

    private class DndTask extends AsyncTask<Void, Void, List<DndData>>{

        @Override
        protected List<DndData> doInBackground(Void... params) {
            List<DndData> data = null;
            try {
                dndStatusApi = new DndStatusApi();
                data = dndStatusApi.dndDataList();
            }catch (Exception e){
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<DndData> result) {
            super.onPostExecute(result);
            if (result.size() > 0){
                DndData dndData = result.get(0);
                dndNumber.setText(dndData.getMobilenumber());
                status.setText("Status : " + dndData.getDndStatus());
                if (dndData.getDndStatus().contains("off")){
                    activate.setVisibility(View.VISIBLE);
                }
                activate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), SelectDndPlanActivity.class);
                        startActivity(intent);
                        //showAlert();
                    }
                });
            }
        }
    }

  /*  protected void sendSMSMessage() {

        //TODO change number to 1909

        String phoneNo = "9910421820";
        String message = "START 0";

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS failed, please try again.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void showAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DndActivity.this);
        alertDialog.setTitle("Sending SMS");
        alertDialog.setMessage("Additional carrier charges may apply for sending SMS. Do you want to continue?");

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                sendSMSMessage();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.create().show();
    }*/
}
