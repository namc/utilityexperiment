package com.battery.atm;

import android.net.Uri;

import com.battery.main.GetRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 2/11/15.
 */
public class AtmLocator {

    double lat = AtmActivity.lat;
    double lon = AtmActivity.lon;
    String name = AtmActivity.name;
    GetRequest getRequest = new GetRequest();


    public AtmLocator(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public List<AtmData> locationDataList() throws Exception{
        List<AtmData> atmDatas = new ArrayList<AtmData>();
        Uri.Builder builder = Uri.parse("https://maps.googleapis.com/maps/api/place/nearbysearch/json").buildUpon();
        builder.appendQueryParameter("location", String.valueOf(lat)+","+String.valueOf(lon))
                .appendQueryParameter("radius", "2000")
                .appendQueryParameter("types" , "atm")
                .appendQueryParameter("name" , name)
                .appendQueryParameter("key", "***");

        String url = builder.build().toString();
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject json;

        try {
            json = new JSONObject(outputData);
            JSONArray jsonArray = json.getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                AtmData atmData = new AtmData();
                atmData.setName(jsonObject.getString("name"));
                JSONObject geometry = jsonObject.getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                atmData.setLat(location.getDouble("lat"));
                atmData.setLon(location.getDouble("lng"));
                atmData.setAddress(jsonObject.getString("vicinity"));
                atmDatas.add(atmData);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return atmDatas;
    }
}
