package com.battery.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.battery.atm.SelectBankActivity;
import com.battery.dnd.DndActivity;
import com.battery.flight.EnterFlightDataActivity;
import com.battery.foodapp.FoodLocation;
import com.battery.ordertracking.SelectCompanyActivity;
import com.battery.phone.usagemonitor.SelectDateActivity;
import com.battery.phone.ussd.AircelUssdActivity;
import com.battery.phone.ussd.AirtelUssdActivity;
import com.battery.phone.ussd.IdeaUssdActivity;
import com.battery.phone.ussd.RelianceUssdActivity;
import com.battery.phone.ussd.TataDocomoUssdActivity;
import com.battery.phone.ussd.VodafoneUssdActivity;
import com.battery.pricecompare.EnterItemActivity;
import com.battery.train.pnr.EnterPnrActivity;
import com.battery.ubertestapp.CabListActivity;
import com.battery.ubertestapp.R;

/**
 * Created by namrata on 2/9/15.
 */
public class MainActivity extends Activity {

    Button cab;
    Button food;
    Button track;
    Button atm;
    Button ussd;
    Button phoneusage;
    Button train;
    Button priceCompare;
    Button flight;
    Button dnd;
    GpsService gpsService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cab = (Button) findViewById(R.id.cab_activity);
        food = (Button) findViewById(R.id.food_activity);
        track = (Button) findViewById(R.id.tracking_activity);
        atm = (Button) findViewById(R.id.atm_activity);
        ussd = (Button) findViewById(R.id.phone_ussd_activity);
        phoneusage = (Button) findViewById(R.id.phone_usage_activity);
        train = (Button) findViewById(R.id.train_activity);
        priceCompare = (Button) findViewById(R.id.price_activity);
        flight = (Button) findViewById(R.id.flight_activity);
        dnd = (Button) findViewById(R.id.dnd_activity);

        gpsService = new GpsService(MainActivity.this);

        if (gpsService.canGetLocation()){
            cab.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(getApplicationContext(), CabListActivity.class);
                    startActivity(intent);
                }
            });
            food.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(getApplicationContext(), FoodLocation.class);
                    startActivity(intent);
                }
            });
            atm.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(getApplicationContext(), SelectBankActivity.class);
                    startActivity(intent);
                }
            });

        } else {
            gpsService.showSettingsAlert();
        }
        ussd.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                TelephonyManager manager = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                String carrierName = manager.getNetworkOperatorName().toUpperCase();
                if (carrierName.contains("AIRTEL")){
                    Intent intent = new Intent(getApplicationContext(), AirtelUssdActivity.class);
                    intent.putExtra("Carrier" , carrierName);
                    startActivity(intent);
                } else if (carrierName.contains("VODAFONE")){
                    Intent intent = new Intent(getApplicationContext(), VodafoneUssdActivity.class);
                    intent.putExtra("Carrier" , carrierName);
                    startActivity(intent);
                }else if (carrierName.contains("AIRCEL")){
                    Intent intent = new Intent(getApplicationContext(), AircelUssdActivity.class);
                    intent.putExtra("Carrier" , carrierName);
                    startActivity(intent);

                }else if (carrierName.contains("TATA")){
                    Intent intent = new Intent(getApplicationContext(), TataDocomoUssdActivity.class);
                    intent.putExtra("Carrier" , carrierName);
                    startActivity(intent);

                }else if (carrierName.contains("RELIANCE")){
                    Intent intent = new Intent(getApplicationContext(), RelianceUssdActivity.class);
                    intent.putExtra("Carrier" , carrierName);
                    startActivity(intent);

                }else if (carrierName.contains("Idea")){
                    Intent intent = new Intent(getApplicationContext(), IdeaUssdActivity.class);
                    intent.putExtra("Carrier" , carrierName);
                    startActivity(intent);

                } else {
                    Toast.makeText(getApplicationContext(), "Service not available for your Service provider " + carrierName ,Toast.LENGTH_SHORT).show();
                }
            }
        });
        phoneusage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), SelectDateActivity.class);
                startActivity(intent);
            }
        });

        train.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), EnterPnrActivity.class);
                startActivity(intent);
            }
        });

        track.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), SelectCompanyActivity.class);
                startActivity(intent);
            }
        });

        priceCompare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EnterItemActivity.class);
                startActivity(intent);
            }
        });

        flight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EnterFlightDataActivity.class);
                startActivity(intent);
            }
        });

        dnd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
                String number = tm.getLine1Number();
                //Toast.makeText(getApplicationContext(), "Number "  + number , Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), DndActivity.class);
                intent.putExtra("number" , number);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        gpsService = new GpsService(MainActivity.this);
    }
}

