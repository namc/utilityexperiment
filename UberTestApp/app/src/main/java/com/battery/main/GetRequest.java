package com.battery.main;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by namrata on 2/25/15.
 */
public class GetRequest {

    public String GET(String url) throws Exception{
        //create HTTPClient
        HttpClient httpclient = new DefaultHttpClient();
        org.apache.http.client.methods.HttpGet request = new org.apache.http.client.methods.HttpGet(url);

        HttpResponse httpResponse = httpclient.execute(request);

        System.out.println("Response Code : "
                + httpResponse.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(httpResponse.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine())!=null){
            result.append(line);
        }
        return result.toString();
    }

}
