package com.battery.foodapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.battery.main.GpsService;
import com.battery.ubertestapp.R;


/**
 * Created by namrata on 2/10/15.
 */

public class FoodLocation extends Activity {
    EditText name;
//    EditText locality;
    Button nearMe;
//    Button findInLocality;
    GpsService gps;
    public double lat , lon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_location);
        name = (EditText)findViewById(R.id.name);
//        locality = (EditText)findViewById(R.id.locality);
        nearMe = (Button)findViewById(R.id.near_me);
//        findInLocality = (Button)findViewById(R.id.find_in_locality);
        gps = new GpsService(FoodLocation.this);

        nearMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gps.canGetLocation()){
                    lat = gps.getLatitude();
                    lon = gps.getLongitude();
                    Intent intent = new Intent(getApplicationContext(), RestaurantsActivity.class);
                    intent.putExtra("Name" , name.getText().toString());
                    intent.putExtra("Lat" , lat);
                    intent.putExtra("Lon" , lon);
                    startActivity(intent);
                }
            }
        });

/*        findInLocality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RestaurantsActivity.class);
                intent.putExtra("Name" , name.getText().toString());
                intent.putExtra("Locality" , locality.getText().toString());
                startActivity(intent);
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        gps = new GpsService(FoodLocation.this);
    }
}
