package com.battery.ordertracking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.battery.ubertestapp.R;

/**
 * Created by namrata on 2/18/15.
 */
public class SelectCompanyActivity extends Activity {

    Button flipkart;
    Button jabong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_company);
        flipkart = (Button) findViewById(R.id.ecom_1);
        jabong = (Button) findViewById(R.id.ecom_2);

        flipkart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FlipKartOrderStatus.class);
                startActivity(intent);
            }
        });

        jabong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), JabongOrderStatus.class);
                startActivity(intent);
            }
        });
    }
}
