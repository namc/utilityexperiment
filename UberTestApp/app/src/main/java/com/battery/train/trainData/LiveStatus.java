package com.battery.train.trainData;

import android.net.Uri;

import com.battery.main.GetRequest;
import com.battery.train.PnrActivity;
import com.battery.train.PnrData;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 2/23/15.
 */
public class LiveStatus {

    String trainno = PnrActivity.trainno;
    String stnfrom = PnrActivity.stnfrom;
    String date = PnrActivity.date;
    GetRequest getRequest = new GetRequest();


    public List<PnrData> liveStatusData() throws Exception{
        List<PnrData> livePnrDatas = new ArrayList<PnrData>();
        Uri.Builder builder = Uri.parse("http://api.erail.in/live").buildUpon();
        builder.appendQueryParameter("key", "***")
                .appendQueryParameter("trainno" , trainno)
                .appendQueryParameter("stnfrom" , stnfrom)
                .appendQueryParameter("date" , date);

        String url = builder.build().toString();
        String outputData =getRequest.GET(url);
        System.out.println(outputData);
        System.out.println(url);
        JSONObject jsonObject;

        try{
            jsonObject = new JSONObject(outputData);
            PnrData pnrData = new PnrData();

            JSONObject result = jsonObject.getJSONObject("result");
            pnrData.setTrainNum(result.getString("trainno"));
            pnrData.setTrainName(result.getString("name"));
            pnrData.setDelayrun(result.getString("delayrun"));
            pnrData.setDate(result.getString("date"));
            pnrData.setFromName(result.getString("fromname"));
            pnrData.setScharr(result.getString("scharr"));
            pnrData.setSchdep(result.getString("schdep"));
            pnrData.setActarr(result.getString("actarr"));
            pnrData.setActdep(result.getString("actdep"));
            pnrData.setPlatform(result.getString("platform"));
            pnrData.setSrcname(result.getString("srcname"));
            pnrData.setDestname(result.getString("destname"));
            pnrData.setLastUpdate(result.getString("statusat"));
            pnrData.setDistance(result.getString("disttogo"));
            livePnrDatas.add(pnrData);

        }catch (Exception e){
            e.printStackTrace();
        }

        return livePnrDatas;
    }
}
