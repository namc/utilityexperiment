package com.battery.train.pnr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.battery.train.PnrActivity;
import com.battery.ubertestapp.R;


/**
 * Created by namrata on 2/16/15.
 */
public class EnterPnrActivity extends Activity{

    EditText pnrNumber;
    Button infoByPnr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_pnr);
        pnrNumber = (EditText) findViewById(R.id.enter_pnr);
        infoByPnr = (Button) findViewById(R.id.pnr_info);

        infoByPnr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pnrNumber!=null) {
                    Intent intent = new Intent(getApplicationContext(), PnrActivity.class);
                    intent.putExtra("PNR", pnrNumber.getText().toString());
                    startActivity(intent);
                }else  {
                    Toast.makeText(getApplicationContext() ,"Please enter PNR Number" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
