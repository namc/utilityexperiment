package com.battery.train;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.battery.train.PnrData;
import com.battery.train.pnr.ErailApi;
import com.battery.train.trainData.LiveStatus;
import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/16/15.
 */
public class PnrActivity extends Activity {

    public static String pnr, trainno, stnfrom, date;
    TextView pnrNum;
    TextView live;
    Button route;

    ErailApi erailApi;
    LiveStatus liveStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnr);
        Intent intent = getIntent();
        pnr = intent.getStringExtra("PNR");
        pnrNum = (TextView) findViewById(R.id.pnr);
        live = (TextView) findViewById(R.id.live_status);
        route = (Button) findViewById(R.id.route);
        new PnrTask().execute();
        new LiveStatusTask().execute();

        route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RouteActivity.class);
                startActivity(intent);
            }
        });
    }

    private class PnrTask extends AsyncTask<Void, Void, List<PnrData>> {

        @Override
        protected List<PnrData> doInBackground(Void... params) {
            List<PnrData> data = null;
            try {
                erailApi = new ErailApi();
                data = erailApi.erailPnrDataList();
            }catch (Exception e){
                e.printStackTrace();
            }
            return  data;
        }

        @Override
        protected void onPostExecute(List<PnrData> result) {
            super.onPostExecute(result);
            if (result.size() > 0) {
                PnrData pnrData = result.get(0);
                trainno = pnrData.getTrainNum();
                stnfrom = pnrData.getFromStation();
                date = pnrData.getDate();
                pnrNum.setText("PNR Number : " + pnrData.getPnrNum()
                    + "\n" + "Train Number : " + pnrData.getTrainNum()
                    + "\n" + "Train Name : " + pnrData.getTrainName()
                    + "\n" + "Date of Journey : " + pnrData.getDate()
                    + "\n" + "From : " + pnrData.getFromStation()
                    + "\n" + "To : " + pnrData.getToStation()
                    + "\n" + "Boarding Point : " + pnrData.getBoardingPoint()
                    + "\n" + "Train Class : " + pnrData.getTrainClass());

            }
        }
    }

    private class LiveStatusTask extends AsyncTask<Void, Void, List<PnrData>> {

        @Override
        protected List<PnrData> doInBackground(Void... params) {
            List<PnrData> data = null;
            try {
                liveStatus = new LiveStatus();
                data = liveStatus.liveStatusData();
            }catch (Exception e){
                e.printStackTrace();
            }
            return  data;
        }

        @Override
        protected void onPostExecute(List<PnrData> result) {
            super.onPostExecute(result);
            if (result.size() > 0) {
                PnrData pnrData = result.get(0);
                live.setText("Last Updated at : " + pnrData.getLastUpdate()
                    + "\n" + "Train Number : " + pnrData.getTrainNum()
                    + "\n" + "Train Name : " + pnrData.getTrainName()
                    + "\n" + "Delay Run : " + pnrData.getDelayrun()
                    + "\n" + "Date of Journey : " + pnrData.getDate()
                    + "\n" + "From : " + pnrData.getFromName()
                    + "\n" + "To : " + pnrData.getDestname()
                    + "\n" + "Boarding Point : " + pnrData.getSrcname()
                    + "\n" + "Scheduled Arrival : " + pnrData.getScharr()
                    + "\n" + "Actual Arrival : " + pnrData.getActarr()
                    + "\n" + "Scheduled Departure : " + pnrData.getSchdep()
                    + "\n" + "Actual Departure : " + pnrData.getActdep()
                    + "\n" + "Platform : " + pnrData.getPlatform()
                    + "\n" + "Distance : " + pnrData.getDistance());
            }
        }
    }
}
