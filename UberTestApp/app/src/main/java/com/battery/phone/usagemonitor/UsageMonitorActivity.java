package com.battery.phone.usagemonitor;

import android.app.Activity;
import android.content.Intent;
import android.net.TrafficStats;
import android.os.Bundle;
import android.widget.TextView;

import com.battery.ubertestapp.R;

import java.util.List;

/**
 * Created by namrata on 2/19/15.
 */

public class UsageMonitorActivity extends Activity {
    TextView usage;
    TextView internet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_usage);
        Intent intent = getIntent();
        long startDate = intent.getLongExtra("startDate", 0);
        long endDate = intent.getLongExtra("endDate", 0);
        List<CallLogData> callLogDatas = CallLogHelper.getAllCallLogs(getContentResolver(), startDate, endDate);
        long val = 0;
        for (CallLogData callLogData : callLogDatas){
            if (callLogData.logType== 2){
                val = val + callLogData.getLogTime();
            }
        }
        long mobUpload = TrafficStats.getMobileTxBytes();
        long mobDown = TrafficStats.getMobileRxBytes();
        long mobTot = (mobUpload+mobDown)/1048576;
        usage = (TextView) findViewById(R.id.duration);
        internet = (TextView) findViewById(R.id.internet);
        usage.setText("Call duration : " + val + " seconds");
        internet.setText("Internet Usage in MBs : " + mobTot);
    }
}
