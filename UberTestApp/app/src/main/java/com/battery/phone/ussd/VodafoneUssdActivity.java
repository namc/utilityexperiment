package com.battery.phone.ussd;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.battery.ubertestapp.R;

/**
 * Created by namrata on 2/12/15.
 */
public class VodafoneUssdActivity extends Activity {


    Button mainBalance;
    Button internetBalance;
    Button smsBalance;
    TextView carrierName;
    public static String carrier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_ussd);
        carrierName = (TextView) findViewById(R.id.carried_name);
        Intent intent = getIntent();
        carrier = intent.getStringExtra("Carrier");
        carrierName.setText(carrier);
        mainBalance = (Button) findViewById(R.id.main_balance);
        internetBalance = (Button) findViewById(R.id.internet_balance);
        smsBalance = (Button) findViewById(R.id.sms_balance);

        mainBalance.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String ussdCode = "*" + "141" + Uri.encode("#");
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
            }
        });

        internetBalance.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String ussdCode = "*" + "111" + "*" + "6" +"*" +"2" + Uri.encode("#");
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
            }
        });

        smsBalance.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String ussdCode = "*" + "142" + Uri.encode("#");
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
            }
        });
    }
}
