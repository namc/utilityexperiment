package com.battery.pricecompare;

import android.net.Uri;

import com.battery.main.GetRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by namrata on 2/24/15.
 */

public class PriceApi {

    String query = PriceCompareActivity.query;
    GetRequest getRequest = new GetRequest();

    public List<ItemData> itemDataList() throws Exception {
        List<ItemData> itemDatas = new ArrayList<ItemData>();
        Uri.Builder builder = Uri.parse("http://compare.buyhatke.com/searchEngine2.php").buildUpon();
        builder.appendQueryParameter("searchQuery" , query)
                .appendQueryParameter("platform" , "android")
                .appendQueryParameter("app_id" , "36242")
                .appendQueryParameter("app_auth" , "667786008");

        String url = builder.build().toString();
        System.out.println(url);
        String outputData = getRequest.GET(url);
        System.out.println(outputData);
        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(outputData);
            for (int i = 0; i<jsonArray.length(); i++){
                JSONObject itemInfo = jsonArray.getJSONObject(i);
                ItemData itemData = new ItemData();
                itemData.setProdName(itemInfo.getString("prod"));
                itemData.setPrice(itemInfo.getString("price"));
                itemData.setRating(itemInfo.getLong("seller_review"));
                itemData.setSiteName(itemInfo.getString("siteName"));
                String imgUrl = itemInfo.getString("image").replaceAll("\\\\", "");
                itemData.setImageUrl(imgUrl);
                itemDatas.add(itemData);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return itemDatas;
    }
}
