package com.battery.ubertestapp;

import java.util.List;

/**
 * Created by namrata on 1/30/15.
 */
public class LocationData {

    public String name;

    public String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
