package com.battery.ubertestapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class CabListActivity extends Activity {

    APICall apiCall;

    public static double x, y;

    Timer timer;
    Timer timerAsync;
    LocationManager lm;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    CabAdapter cabAdapter;
    ListView view;
    Location location;
    public static final int NOTIFICATION_ID = 1;

    private Handler handler = new Handler();
    private void postDataSetChangedNotification(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                cabAdapter.notifyDataSetChanged();
            }
        });
    }
    private void clearAdapter(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                cabAdapter.clear();
            }
        });
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        view = (ListView) findViewById(R.id.cabList);
        List<CabData> result = new ArrayList<>();
        cabAdapter = new CabAdapter(CabListActivity.this, 0, result);
        view.setAdapter(cabAdapter);


        if (gps_enabled)
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    locationListenerGps);
        if (network_enabled)
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
                    locationListenerNetwork);
        timer = new Timer();
//        timer.schedule(new GetLastLocation(), 10000);
        timer.scheduleAtFixedRate(new GetLastLocation(), 10000, 10000);

        timerAsync = new Timer();

        timerAsync.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                new APITask().execute();
            }
        }, 100, 20000);

        timerAsync.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                new ReverseGeocodingTask(getBaseContext()).execute(location);
            }
        }, 1, 10000);

        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PackageManager pm = getPackageManager();
                CabAdapter uberAdapter1 = (CabAdapter) parent.getAdapter();
                CabData cabData = (CabData) uberAdapter1.getItem(position);
                String productID = cabData.productId;
                String cabCompany = cabData.cabCompany;

                if (cabCompany.equals(CabData.CAB_UBER)) {
                    try {
                        pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
                        //make new intent and launch UberApp
                        Uri uri = Uri.parse("uber://?client_id=q7qpOQVGadGGR14a_sWBBLH3NCy1tfak&action=setPickup" + "&productID=" + productID);


                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (PackageManager.NameNotFoundException e) {
                        //Take user to PlayStore
                        Uri uri = Uri.parse("market://search?q=pname:com.ubercab");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                } else if (cabCompany.equals(CabData.CAB_OLA)) {
                    try {
                        pm.getPackageInfo("com.olacabs.customer", PackageManager.GET_ACTIVITIES);
                        Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.olacabs.customer");
                        startActivity(LaunchIntent);

                    } catch (PackageManager.NameNotFoundException e) {
                        //Take user to PlayStore
                        Uri uri = Uri.parse("market://search?q=pname:com.olacabs.customer");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                }
                else if (cabCompany.equals(CabData.CAB_TFS)){
                    try {
                        pm.getPackageInfo("com.tfs.consumer" , PackageManager.GET_ACTIVITIES);
                        Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.tfs.consumer");
                        startActivity(LaunchIntent);

                    }catch (PackageManager.NameNotFoundException e){
                        Uri uri = Uri.parse("market://search?q=pname:com.tfs.consumer");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                }
                else if (cabCompany.equals(CabData.CAB_MERU)){
                    try {
                        pm.getPackageInfo("com.winit.merucab" , PackageManager.GET_ACTIVITIES);
                        Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.winit.merucab");
                        startActivity(LaunchIntent);

                    }catch (PackageManager.NameNotFoundException e){
                        Uri uri = Uri.parse("market://search?q=pname:com.winit.merucab");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null || timerAsync != null) {
            timer.cancel();
            timerAsync.cancel();
        }
    }

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer.cancel();
            x = location.getLatitude();
            y = location.getLongitude();
            lm.removeUpdates(this);
            lm.removeUpdates(locationListenerNetwork);

            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, "gps enabled " + x + "\n" + y, duration);
            toast.show();
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer.cancel();
            x = location.getLatitude();
            y = location.getLongitude();
            lm.removeUpdates(this);
            lm.removeUpdates(locationListenerGps);

            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, "Network enabled", duration);
            toast.show();
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    private class APITask extends AsyncTask<Void, Void, List<CabData>> {
        List<CabData> data;
        List<LocationData> loc_data;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            cabAdapter.clear();
            clearAdapter();
        }

        @Override
        protected List doInBackground(Void... params) {

            try {
                Thread.sleep(4000);
                if (gps_enabled) {
                    if (lm.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {

                        apiCall = new APICall(lm.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                    } else {
                        apiCall = new APICall(x, y);
                    }
                } else {
                    if (lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
                        apiCall = new APICall(lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
                    } else {
                        apiCall = new APICall(x, y);
                    }
                }

                // Add all cab data one by one
                cabAdapter.addCabs(apiCall.ubercabList());
                postDataSetChangedNotification();
                cabAdapter.addCabs(apiCall.olacabList());
                postDataSetChangedNotification();
                cabAdapter.addCabs(apiCall.tfscabList());
                postDataSetChangedNotification();
                cabAdapter.addCabs(apiCall.merucabList());
                postDataSetChangedNotification();
                loc_data = apiCall.locationDataList();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(List<CabData> result) {
            super.onPostExecute(result);
            if (data != null) {
                /*cabAdapter = new CabAdapter(CabListActivity.this, 0, data);
                view.setAdapter(cabAdapter);*/
            } else {
                Toast.makeText(CabListActivity.this, "No data", Toast.LENGTH_LONG).show();
            }
            // Hail a cab notification happens here.
            if (loc_data!=null){
                for (LocationData data1:loc_data){
                    if (data1.getType().contains("establishment") || data1.getType().contains("airport")){
                        sendNotification();
                    }
                }
            }
            return;
        }
    }

    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
            lm.removeUpdates(locationListenerGps);
            lm.removeUpdates(locationListenerNetwork);

            Location net_loc = null, gps_loc = null;
            if (gps_enabled)
                gps_loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (network_enabled)
                net_loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            //if there are both values use the latest one
            if (gps_loc != null && net_loc != null) {
                if (gps_loc.getTime() > net_loc.getTime()) {
                    x = gps_loc.getLatitude();
                    y = gps_loc.getLongitude();
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, "gps last known " + x + "\n" + y, duration);
                    toast.show();
                } else {
                    x = net_loc.getLatitude();
                    y = net_loc.getLongitude();
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, "network last known " + x + "\n" + y, duration);
                    toast.show();
                }
            }

            if (gps_loc != null) {
                {
                    x = gps_loc.getLatitude();
                    y = gps_loc.getLongitude();
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, "gps last known " + x + "\n" + y, duration);
                    toast.show();
                }
            }
            if (net_loc != null) {
                {
                    x = net_loc.getLatitude();
                    y = net_loc.getLongitude();
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, "network last known " + x + "\n" + y, duration);
                    toast.show();
                }
            }
            final Context context = getApplicationContext();
            final int duration = Toast.LENGTH_SHORT;

            CabListActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast toast = Toast.makeText(context, "No last known location available", duration);
                    toast.show();

                }
            });
        }
    }

    private class ReverseGeocodingTask extends AsyncTask<Location, Void, String> {
        Context mContext;
        double lat;
        double lon;
        String addressText = "";

        public ReverseGeocodingTask(Context context) {
            super();
            mContext = context;
        }

        @Override
        protected String doInBackground(Location... params) {
            Geocoder geocoder = new Geocoder(mContext);
            if (gps_enabled) {
                if (lm.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                    lat = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
                    lon = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
                } else {
                    lat = x;
                    lon = y;
                }
            } else {
                if (lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
                    lat = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude();
                    lon = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude();
                } else {
                    lat = x;
                    lon = y;
                }
            }
            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocation(lat,lon,1);
            }catch (IOException e){
                e.printStackTrace();
            }
            if (addresses !=null && addresses.size()>0){
                Address address = addresses.get(0);
                addressText = String.format("%s, %s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getLocality(),
                        address.getCountryName());
            }
            return addressText;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (addressText!=null){
                // add data to the
                final TextView textView = (TextView) findViewById(R.id.location_info);
                textView.setText(addressText);
            }
        }
    }

    private void sendNotification() {
        NotificationManager mNotificationManager;
        Intent intent = new Intent(this, CabListActivity.class);

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Cab Alert")
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setAutoCancel(true)
                        .setContentText("You're at Akosha! Hail a cab!")
                        .setStyle(new NotificationCompat.BigTextStyle());

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}